import React, {Component, useState} from 'react';
import { Text, FlatList, ScrollView, TextInput, View, Image, Button } from 'react-native';

function Cat(props,hello) {
  const [isHungary, setIsHungary] = useState(true);
  
  return (
    <View>
      <Text>I am a also {props.name, props.hello} cat! and i am {isHungary ? 'hungary' : 'Full'}</Text>
       <Button 
        onPress = {() => {
          setIsHungary(false);
        }}
        disabled = {!isHungary}
        title={ isHungary ? 'Need Some Food' : 'No thanks!'}
      />

    </View>
  );
}

function InputBox() {
  const [text, setText] = useState('');
  return (
    <View>
      
     <TextInput 
      placeholder="Type here to translate!"
        onChangeText = {text => setText(text)}
        defaultValue = {text}
         style={{ marginTop:20, borderWidth:2, borderRadius:20, height: 70,  backgroundColor: '#ffffff', paddingLeft: 15, paddingRight: 15}}
      />
      <Text >
        {text.split(' ').map((word) => word && '🍕').join(' ')}
      </Text>   
      

    </View>
  );
}

function ListView() {
  const [text, setText] = useState('');
  return (
    <View>
    <FlatList 
      data = {
        [
          {name : 'Ankit'},
          {name : 'Atul'},
          {name : 'Neeraj'},
        ]
      }
      renderItem = {({item}) => <Text>{item.name}</Text>}
    />

    </View>
  );
}


/*function InputBox(props,hello) {
  const [text, setText] = useState('');
  return (
    
  );
}*/


export default function Cafe() {
  
  return (

    <ScrollView>
      <Text>Welcome!</Text>
      <Cat name="qq" hello="-achaaa"/>
      <Cat name="qq"/>
      <Cat name="qq"/>
      <Image 
      source="http://enacteservices.net/paintpad/image/dasboard_logo.png" 
      style={{width:'100%', height:'100px', aspectRatio:1}} 
      />

      <InputBox />
      <InputBox />
      <InputBox />

      <ListView />
      
    </ScrollView>
  );
}
